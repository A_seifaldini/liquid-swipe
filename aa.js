import React from "react";
import { StatusBar } from "react-native";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";

import LiquidSwipe, { assets as liquidSwipeAssets } from "./LiquidSwipe";

const AppNavigator = createAppContainer(
	createStackNavigator(
		{
			LiquidSwipe: {
				screen: LiquidSwipe,
				navigationOptions: {
					title: "Liquid Swipe",
					gesturesEnabled: false
				}
			}
		},
		{
			defaultNavigationOptions: {
				headerStyle: {
					backgroundColor: "white",
					borderBottomWidth: 0
				},
				headerTintColor: "white"
			}
		}
	)
);

export default () => <AppNavigator />;
